<?php


class WithdrawalReceiptStyle
{
	public $phone_number;
	public $amount;
	public $signature;
	public $sign;

   public function __construct($phone_number = '', $amount = '', $signature = '', $sign = '') {
		$this->phone_number = $phone_number;
		$this->amount       = $amount;
		$this->signature    = $signature;
		$this->sign         = $sign;
   }
   
    public function __toString() {
		$phone_number_cols = 24;
		$amount_cols       = 24;
		$signature_cols    = 12;
		$sign_cols         = 12;

		$one   = str_pad($this->phone_number, $phone_number_cols);
		$two   = str_pad($this->amount, $amount_cols);
		$three = str_pad($this->signature, $signature_cols);
		$four  = str_pad($this->sign, $sign_cols);

		return "$one$two\n\n$three$four\n";
    }

}
