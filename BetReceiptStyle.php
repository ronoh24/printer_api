<?php

class BetReceiptStyle
{
	public $date;
	public $game_id;
	public $match;
	public $pick;
	public $odds;

   public function __construct($date = null, $game_id = '', $match = '', $pick = '', $odds = '') {
		$this->date    = $date;
		$this->game_id = $game_id;
		$this->match   = $match;
		$this->pick    = $pick;
		$this->odds    = $odds;
   }
   
    public function __toString() {
		$date_cols    = 6;
		$game_id_cols = 6;
		$match_cols   = 26;
		$pick_cols    = 10;
		$odds_cols    = 6;
		$br_cols      = 48;
		
		$zero  = str_pad($this->date, $date_cols);
		$one   = str_pad($this->game_id, $game_id_cols);
		$two   = str_pad($this->match, $match_cols);
		$three = str_pad($this->pick, $pick_cols);
		$four  = str_pad($this->odds, $odds_cols);
		$five  = "-------------------------------------------------";

		return "$one$two$four\n$zero$pick\n$five\n";
		//return "$zero\n$one$two$three$four\n";
    }
}
