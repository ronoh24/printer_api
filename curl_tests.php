<?php

use \Firebase\JWT\JWT;
require_once 'vendor/firebase/php-jwt/src/JWT.php';
require_once 'configs/configs.php';

$configs = new Configs();
$key = $configs->get_api_key();

$bet = array(
	'shop_name' => 'Premier',
	'bet_id' => 1,
	'bet_amount' => 2000,
	'winnings' => 1030,
	'bonus' => 100,
	'possible_win' => 16400,
	'total_odd' => 10.20,
	'date_created' => "2018-02-12 10:00:00"
);

$slip = array(
	'country' => 'England',
	'competition' => 'Premier League',
	'date' => '20-02-18 10:00:00',
	'game_id' => 4568,
	'market' => '1X2',
	'name' => 'Hull City vs Arsenal',
	'pick' => 2,
	'odd_value' => 2.02
);

$slip = array(
	'country' => 'England',
	'competition' => 'Premier League',
	'date' => '20/02/18 10:00:00',
	'game_id' => 4568,
	'market' => 'Multigoals',
	'name' => 'Wolverhampton Wanderers vs AFC Bournemouth',
	'pick' => '1-5',
	'odd_value' => 2.02
);

$slip1 = array(
	'country' => 'England',
	'competition' => 'Premier League',
	'date' => '20/02/18 10:00:00',
	'game_id' => 2134,
	'market' => '1st Half 1X2 1X2',
	'name' => 'Chelsea vs West Ham',
	'pick' => '1',
	'odd_value' => 2.35
);

$slip2 = array(
	'country' => 'England',
	'competition' => 'Premier League',
	'date' => '20/02/18 10:00:00',
	'game_id' => 3456,
	'market' => 'Total',
	'name' => 'Man U vs Liverpool',
	'pick' => 'Over 2.5',
	'odd_value' => 1.34
);

$slip3 = array(
	'country' => 'England',
	'competition' => 'Premier League',
	'date' => '20/02/18 10:00:00',
	'game_id' => 2030,
	'market' => 'Total',
	'name' => 'Juventus vs Tottenham',
	'pick' => 'Under 2.5',
	'odd_value' => 1.67
);

$bet_slips_info = array(
	'0' => $slip,
	'1' => $slip1,
	'2' => $slip2,
	'3' => $slip3,
);

$payload = array(
	'request' => 'print_betslip',
	'bet' => $bet,
	'bet_slips_info' => $bet_slips_info,
	'language' => 'amh'
);

/*$payload = array(
	'request' => 'print_payment_receipt',
	'bet_id' => 1,
	'bet_amount' => 200
);*/

/*$payload = array(
	'request' => 'print_cancelled_bet_receipt',
	'bet_id' => 1,
	'bet_amount' => 200
);*/

/*$payload = array(
	'request' => 'print_deposit_receipt',
	'msisdn' => "254725939416",
	'amount' => 200
);
*/

/*$payload = array(
	'request' => 'print_withdrawal_receipt',
	'msisdn' => "254725939416",
	'amount' => 200
);*/


$token = JWT::encode($payload, $key);
$token = array(
	"token" => $token
);

$token = json_encode($token);
//die(print_r($token, true));

$curl = curl_init();
CURL_SETOPT_ARRAY($curl, array(
CURLOPT_RETURNTRANSFER => 1,
CURLOPT_VERBOSE => 1,
CURLOPT_HEADER => 1,
CURLOPT_PORT => "",
CURLOPT_URL => "http://localhost/printer_api/index.php",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 30,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "POST",
CURLOPT_POSTFIELDS => "$token",
CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
  ),
));

$response = curl_exec($curl);
$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$header = substr($response, 0, $header_size);
$body = substr($response, $header_size);
$err = curl_error($curl);

curl_close($curl);
die(print_r($body, true));

