<?php 

class PrinterMethods {

	public $printer;

	public function __construct() {
		$this->printer = new ThermalPrinter();
	}

	public function print_bet_receipt($bet_slips_info, $bet) {

		$payload = array(
			'request' => 'print_bet_receipt',
			'bet' => $bet,
			'bet_slips_info' => $bet_slips_info
		);


		$token = JWT::encode($payload, $key);
		$token = array(
			"token" => $token
		);

		$token = json_encode($token);
		//die(print_r($token, true));

		$curl = curl_init();
		CURL_SETOPT_ARRAY($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_VERBOSE => 1,
		CURLOPT_HEADER => 1,
		CURLOPT_PORT => "",
		CURLOPT_URL => "196.207.140.253:5003/printer_api/index.php",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "$token",
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
		  ),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		$err = curl_error($curl);

		curl_close($curl);
	}

	public function print_cancelled_bet_receipt($bet_id, $bet_amount) {

		$payload = array(
			'request' => 'print_cancelled_bet_receipt',
			'bet_id' => $bet_id,
			'bet_amount' => $bet_amount
		);

		$token = JWT::encode($payload, $key);
		$token = array(
			"token" => $token
		);

		$token = json_encode($token);
		//die(print_r($token, true));

		$curl = curl_init();
		CURL_SETOPT_ARRAY($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_VERBOSE => 1,
		CURLOPT_HEADER => 1,
		CURLOPT_PORT => "",
		CURLOPT_URL => "196.207.140.253:5003/printer_api/index.php",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "$token",
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
		  ),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		$err = curl_error($curl);

		curl_close($curl);

	}

	public function print_payment_receipt($bet_id, $bet_amount) {

		$payload = array(
			'request' => 'print_payment_receipt',
			'bet_id' => $bet_id,
			'bet_amount' => $bet_amount
		);

		$token = JWT::encode($payload, $key);
		$token = array(
			"token" => $token
		);

		$token = json_encode($token);
		//die(print_r($token, true));

		$curl = curl_init();
		CURL_SETOPT_ARRAY($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_VERBOSE => 1,
		CURLOPT_HEADER => 1,
		CURLOPT_PORT => "",
		CURLOPT_URL => "196.207.140.253:5003/printer_api/index.php",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "$token",
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
		  ),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		$err = curl_error($curl);

		curl_close($curl);
	}

	public function print_withdrawal_receipt($msisdn, $amount) {

		$payload = array(
			'request' => 'print_withdrawal_receipt',
			'msisdn' => $msisdn,
			'amount' => $amount
		);

		$token = JWT::encode($payload, $key);
		$token = array(
			"token" => $token
		);

		$token = json_encode($token);
		//die(print_r($token, true));

		$curl = curl_init();
		CURL_SETOPT_ARRAY($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_VERBOSE => 1,
		CURLOPT_HEADER => 1,
		CURLOPT_PORT => "",
		CURLOPT_URL => "196.207.140.253:5003/printer_api/index.php",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "$token",
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
		  ),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		$err = curl_error($curl);

		curl_close($curl);
	}

	public function print_deposit_receipt($msisdn, $amount) {


		$payload = array(
			'request' => 'print_deposit_receipt',
			'msisdn' => $msisdn,
			'amount' => $amount
		);

		$token = JWT::encode($payload, $key);
		$token = array(
			"token" => $token
		);

		$token = json_encode($token);
		//die(print_r($token, true));

		$curl = curl_init();
		CURL_SETOPT_ARRAY($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_VERBOSE => 1,
		CURLOPT_HEADER => 1,
		CURLOPT_PORT => "",
		CURLOPT_URL => "196.207.140.253:5003/printer_api/index.php",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "$token",
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
		  ),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		$err = curl_error($curl);

		curl_close($curl);
	}

}