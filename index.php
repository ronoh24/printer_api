<?php 

use \Firebase\JWT\JWT;
require_once 'vendor/firebase/php-jwt/src/JWT.php';
require_once 'logger.php';
require_once 'configs/configs.php';
require_once __DIR__.'/ThermalPrinter.php';

$log = new Logger();
$configs = new Configs();
$key = $configs->get_api_key();



header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin, Content-Type");
header('P3P: CP="CAO PSA OUR"'); // Makes IE to support cookies

// Handling the Preflight
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') { 
    exit();
}

if($_SERVER['REQUEST_METHOD'] === 'POST'):

	$input = file_get_contents('php://input');
	error_log($input);
	$payload = json_decode($input, true);
	$token = $payload['token'];

	$response = JWT::decode($token, $key, array('HS256'));
	//$response = JWT::decode($token, $key);

	$log->info("Received request -> ".$response->request." :: params :: ".print_r($response, true));

	switch ($response->request) {
		case 'print_withdrawal_receipt':
			$msisdn = $response->msisdn;
			$amount = $response->amount;
			$reference_id = $response->reference_id;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_withdrawal_receipt($msisdn, $amount, $reference_id);
		break;

		case 'print_deposit_receipt':
			$msisdn = $response->msisdn;
			$amount = $response->amount;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_deposit_receipt($msisdn, $amount);
		break;

		case 'print_bet_receipt':
			$bet_slips_info = $response->bet_slips_info;
			$bet = $response->bet;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_betslip($bet_slips_info, $bet);
		break;

		case 'print_raw_bet_receipt':
			$bet_slips_info = $response->bet_slips_info;
			$bet = $response->bet;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_betslip($bet_slips_info, $bet);
		break;

		case 'print_betslip':
			$bet_slips_info = $response->bet_slips_info;
			$bet = $response->bet;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_betslip($bet_slips_info, $bet);
		break;

		case 'print_jackpot_betslip':
			$bet_slips_info = $response->bet_slips_info;
			$bet = $response->bet;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_jackpot_betslip($bet_slips_info, $bet);
		break;

		case 'print_sababisha_betslip':
			$bet_slips_info = $response->bet_slips_info;
			$bet = $response->bet;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);

			$printer = new ThermalPrinter();
			$printer->print_sababisha_betslip($bet_slips_info, $bet);
		break;

		case 'print_cancelled_bet_receipt':
			$seq_id     = $response->seq_id ? $response->seq_id : $response->bet_id;
			$bet_id     = $response->bet_id;
			$bet_amount = $response->bet_amount;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);
			
			$printer = new ThermalPrinter();
			$printer->print_cancelled_bet_receipt($bet_id, $bet_amount, $seq_id);
		break;

		case 'print_payment_receipt':
			$seq_id     = $response->seq_id ? $response->seq_id : $response->bet_id;
			$bet_id     = $response->bet_id;
			$bet_amount = $response->bet_amount;
			$acca_bonus = $response->acca_bonus;
			$current_acca_bonus_percentage = $response->current_acca_bonus_percentage;
			$language = $response->language;

			// set language
			$configs->setLanguage($language);
			
			$printer = new ThermalPrinter();
			$printer->print_payment_receipt($bet_id, $bet_amount, $acca_bonus, $current_acca_bonus_percentage, $seq_id);
			$printer->print_payment_receipt($bet_id, $bet_amount, $acca_bonus, $current_acca_bonus_percentage, $seq_id);
		break;
		
		default:
			die(json_encode(['status' => 400, 'message' => 'Bad request.'])); exit();
		break;
	}
else:
	die(json_encode(['status' => 400, 'message' => 'Bad request.'])); exit();
endif;



?>
