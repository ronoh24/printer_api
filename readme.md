# Printer Api
- the printer api is designed to printer receipts generated from the shop portal features below;
	- deposit receipts
	- place bet receipts
	- cancel bet receipts
	- won bet payouts receipts
	- withdrawals receipts

## technologies
	- php version 7.2+
	- jwt
	- escpos-php library - https://github.com/mike42/escpos-php



## How To Set Up The Application - Installation

### pre requisites
	- apache web server installed
	- php version 7.2+ installed
	- php php-opcache php-mbstring php-memcached php-gd php-common php-bcmath php-imagick php-json php-curl dependencies installed 

### set up the application
#### 1. clone the repository to the web server root folder and checkout the country feature branch
	- `git clone https://bitbucket.org/betikateam/printer_api`
	- `git checkout feature/country`

#### 2. create apache virtual host for printer api and restart apache service
	- `printf 'Listen 80 \n Listen 5000 \n <VirtualHost *:5000> \n DocumentRoot "/var/www/html/printer_api/index.php" \n Servername printer_api \n </VirtualHost>' | tee /etc/apache2/ports.conf`
	- sudo service apache2 restart

#### 3. set up application configs
	- `cd printer_api`
	- `vim config/configs.ini`
	- sample configs.ini file - https://bitbucket.org/betikateam/printer_api/configs/configs.ini

#### 4. create printer usb permissions file
	- `printf '#!/bin/bash \n chmod -R 777 /dev/usb/' | tee /home/betika/start.sh`
	- `chmod +x /home/betika/start.sh`

#### 5. schedule cron for auto printer usb permissions
	- `(crontab -l 2>/dev/null || true; echo "* * * * * /home/betika/start.sh") | crontab -`

#### 6. create application logs folder
	- `mkdir /var/log/printer_api`
	- `chmod -R 777 /var/log/printer_api`

## Testing
	- see sample payloads in PrinterMethods.php
	- use the payload in curl_tests.php to test the application
	- to check logs;
		- `tail -f /var/log/apache2/error.log`
		- `tail -f /var/log/printer_api/*.log`

## Troubleshooting
	- printer machine not printing out receipts
		- check printer port 
			- `ls /dev/usb/`
		- confirm printer_url configs
			- `cat /var/www/html/printer_api/configs/configs.ini`
		- disconnect the escpos printer usb cable from the printer machine
		- switch off and unplug power cable escpos printer from wall socker
		- switch on the escpos printer
		- connect escpos printer usb cable to printer machine
		- check printer port 
			- `ls /dev/usb/`
		- confirm printer_url configs
			- `cat /var/www/html/printer_api/configs/configs.ini`
		- check if permissions crontab file `/home/betika/start.sh` is being executed
			- `tail -f /var/log/syslog`
		- if not running then recreate the `/home/betika/start.sh` with the step number 4 on installation
		- clear browser cache cookies and login to the shop portal
		- confirm the local host printer port is selected
		- if still not working reinstall the printer api application with the commands file below;
			- https://bitbucket.org/betikateam/printer_api/src/aaf436531115eca5f6d2d3fab638e10135b3549a/install.sh?at=feature%2Fmalawi


