<?php

$payload = array(
	"request" => "print-deposit_receipt",
	"msisdn" => "254725939416",
	"amount" => 100
);

$payload = json_encode($payload);

$curl = curl_init();
CURL_SETOPT_ARRAY($curl, array(
CURLOPT_RETURNTRANSFER => 1,
CURLOPT_VERBOSE => 1,
CURLOPT_HEADER => 1,
CURLOPT_PORT => "",
CURLOPT_URL => "localhost/printer_api/index.php",
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 30,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "POST",
CURLOPT_POSTFIELDS => "$payload",
CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 54a53da8-3217-88e7-0ab7-58a28753d685"
  ),
));

$response = curl_exec($curl);
$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$header = substr($response, 0, $header_size);
$body = substr($response, $header_size);
$err = curl_error($curl);

curl_close($curl);

die(print_r($body, true));

