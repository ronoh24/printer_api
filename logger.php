<?php 

class Logger {

	public $info_log_file; 
	public $info_log_level;

	public $error_log_file;
	public $error_log_level;

	public function __construct() {
		$configs = new Configs();

		$this->info_log_file  =  $configs->get_info_log_file();
		$this->info_log_level =  $configs->get_info_log_level();

		$this->error_log_file  = $configs->get_error_log_file();
		$this->error_log_level = $configs->get_error_log_level();
	}

	public function info($message) {
		$fp = fopen($this->info_log_file, "ab+");

		if($fp) {
			fwrite($fp, date('Y-m-d H:i:s'). " " .$this->info_log_level ." :: ". $message);
			fwrite($fp, "\n");
			fclose($fp);
		}
	}

	public function error($message) {
		$fp = fopen($this->error_log_file, "ab+");

		if($fp) {
			fwrite($fp, date('Y-m-d H:i:s'). " " .$this->error_log_level ." :: ". $message);
			fwrite($fp, "\n");
			fclose($fp);
		}
	}
}