<?php

class PaymentReceiptStyle
{
    public $bet_id;
   	public $amount;
   	public $acca_bonus;
   	public $current_acca_bonus_percentage;
   	public $signature;
   	public $sign;

   	public function __construct($bet_id = '', $amount = '', $acca_bonus = '', $current_acca_bonus_percentage = '', $signature = '', $sign = '') {
		$this->bet_id  							= $bet_id;
		$this->amount      	 					= $amount;
		$this->acca_bonus   					= $acca_bonus;
		$this->current_acca_bonus_percentage   	= $current_acca_bonus_percentage;
		$this->signature    					= $signature;
		$this->sign         					= $sign;
    }
   
    public function __toString() {
		$bet_id_cols       = 24;
		$amount_cols       = 22;
		$acca_bonus_cols   = 22;
		$signature_cols    = 12;
		$sign_cols         = 12;

		$one   	= str_pad($this->bet_id, $bet_id_cols);
		$two   	= str_pad($this->amount, $amount_cols);
		$three  = str_pad($this->acca_bonus, $acca_bonus_cols);
		$four 	= str_pad($this->signature, $signature_cols);
		$five  	= str_pad($this->sign, $sign_cols);

		return "$one$two$three\n\n$four$five\n";
    }
}
