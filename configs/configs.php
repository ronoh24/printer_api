<?php

define('LOCALES_PATH', __DIR__.'/locales.json');
define('DEFAULT_LOCALE', 'en');




class Configs {

	public $configs;
	public $locale;
	public $locales;
	public $strings;

	public function __construct() {
		$this->configs  = parse_ini_file('configs.ini', true);
		var_dump( parse_ini_file('configs.ini'));
		$this->locales  = json_decode(file_get_contents(LOCALES_PATH), true); // Get available locales from file
        $this->locale   = isset($_COOKIE['shop-printer-language']) ? $_COOKIE['shop-printer-language'] : $this->locale = DEFAULT_LOCALE; // Set if exists otherwise set default
        $this->strings  = $this->locales[$this->locale]; // Set strings belonging to the locale
	}

	public function get_api_key() {
		$api_key = $this->configs['APP']['api_key'];
		return $api_key;
	}

	public function get_printer_url() {
		$printer_url = $this->configs['APP']['printer_url'];
		return $printer_url;
	}

	public function getPrinterPort() {
		$printer_port = $this->configs['APP']['printer_port'];
		return $printer_port;
	}

	public function get_info_log_file() {
		$info_log_file = $this->configs['APP']['info_log_file'];
		return $info_log_file;
	}

	public function get_info_log_level() {
		$info_log_level = $this->configs['APP']['info_log_level'];
		return $info_log_level;
	}

	public function get_error_log_file() {
		$error_log_file = $this->configs['APP']['error_log_file'];
		return $error_log_file;
	}

	public function get_error_log_level() {
		$error_log_level = $this->configs['APP']['error_log_level'];
		return $error_log_level;
	}

	public function getString($key) {
		return $this->strings[$key];
	}

	public function getCurrency() {
		$currency = $this->configs['APP']['currency'];
		return $currency;
	}

	public function getTimezone() {
		$timezone = $this->configs['APP']['timezone'];
		return $timezone;
	}

	public function getLogo() {
		$logo = $this->configs['APP']['logo'];
		return $logo;
	}

	public function getTaxEnabled() {
		$tax_enabled = $this->configs['APP']['tax_enabled'];
		return $tax_enabled;
	}

	public function getAccaBonusEnabled() {
		$acca_bonus_enabled = $this->configs['APP']['acca_bonus_enabled'];
		return $acca_bonus_enabled;
	}

	public function getVatEnabled() {
		$vat_enabled = $this->configs['APP']['vat_enabled'];
		return $vat_enabled;
	}

	public function getTaxRate() {
		$tax_rate = (float) $this->configs['APP']['tax_rate'];
		return $tax_rate;
	}

	public function getMaxPayout() {
		$max_payout = (float) $this->configs['APP']['max_payout'];
		return $max_payout;
	}

	public function getMaxWinnings() {
		$max_winnings = (float) $this->configs['APP']['max_winnings'];
		return $max_winnings;
	}

	public function getMaxWinningsTax() {
		$max_winnings_tax = (float) $this->configs['APP']['max_winnings_tax'];
		return $max_winnings_tax;
	}

	public function getMaxTotalWinnings() {
		$max_total_winnings = (float) $this->configs['APP']['max_total_winnings'];
		return $max_total_winnings;
	}

	public function setLanguage($language) {
        $cookie_name  = 'shop-printer-language';
        $cookie_value = $language;
        setcookie ($cookie_name, $cookie_value, time() + (86400 * 30), '/', NULL, 0); 
        $_COOKIE['shop-printer-language'] = $cookie_value;
        return true;
	}

	public function getLanguage() {
        // check language from cookie
		if(isset($_COOKIE['shop-printer-language']) && !empty($_COOKIE['shop-printer-language'])):
			$shopLanguage = $_COOKIE['shop-printer-language'];
		else:
			$shopLanguage = 'en';
		endif;

        return $shopLanguage;
    }
}


?>