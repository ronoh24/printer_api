<?php


class CancelBetReceiptStyle
{
	public $bet_amount;
	public $signature;
	public $sign;

   public function __construct($bet_amount = '', $signature = '', $sign = '') {
		$this->bet_amount   = $bet_amount;
		$this->signature    = $signature;
		$this->sign         = $sign;
   }
   
    public function __toString() {
		$bet_amount_cols   = 24;
		$signature_cols    = 12;
		$sign_cols         = 12;

		$two   = str_pad($this->bet_amount, $bet_amount_cols);
		$three = str_pad($this->signature, $signature_cols);
		$four  = str_pad($this->sign, $sign_cols);

		return "$two\n\n$three$four\n";
    }
}
