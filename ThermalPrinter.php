<?php


require __DIR__.'/vendor/mike42/escpos-php/autoload.php';
require __DIR__.'/BetReceiptStyle.php';
require __DIR__.'/CancelBetReceiptStyle.php';
require __DIR__.'/DepositReceiptStyle.php';
require __DIR__.'/PaymentReceiptStyle.php';
require __DIR__.'/WithdrawalReceiptStyle.php';
require_once 'configs/configs.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;


class ThermalPrinter
{
    public $connector;
    public $configs;

    public function __construct() {
        $this->configs = new Configs();
        var_dump($this->configs);
        $printer_url = $this->configs->get_printer_url();
        $this->connector = new FilePrintConnector($printer_url);
        date_default_timezone_set($this->configs->getTimezone());
    }

    public function print_betslip($bet_slips_info, $bet) {

        $shop_name    = $bet->shop_name;
        $bet_id       = $bet->bet_id;
        $bet_amount   = $bet->bet_amount;
        $total_odd    = $bet->total_odd;
        $winnings     = $bet->winnings;
        $bonus        = $bet->bonus;
        $possible_win = $bet->possible_win;
        $date_created = $bet->date_created;
        $date         = $bet->date;
        $acca_bonus   = $bet->acca_bonus;
        $current_acca_bonus_percentage = $bet->current_acca_bonus_percentage;


        //$date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        $logo       = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $bar_big    = EscposImage::load("/var/www/html/printer_api/bar-big.png", false);
        $bar2_big   = EscposImage::load("/var/www/html/printer_api/bar2-big.png", false);
        $bar3_big   = EscposImage::load("/var/www/html/printer_api/bar3-big.png", false);
        $printer    = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);


        $printer -> setTextSize(1, 1);
        $printer->text("\n\n");

        /*$printer -> setEmphasis(true);
        $printer->text($this->configs->getString('JUKWAA_LA_MABINGWA'));
        $printer->text("\n\n");
        $printer -> setEmphasis(false);*/

        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $shop_name = $this->configs->getString('SHOP_NAME_LABEL').$shop_name;
        $printer->text($shop_name);
        $printer->text("\n");

        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $bet_id = $this->configs->getString('BET_ID_LABEL').$bet_id;
        $printer->text($bet_id);
        $printer->text("\n");

        $date_created = date_create($date_created);
        date_timezone_set($date_created, timezone_open($this->configs->getTimezone()));
        $date_created = $date_created->format('Y-m-d H:i:s');
        
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $date_created = $this->configs->getString('DATE_LABEL').$date_created;
        $printer->text($date_created);
        $printer->text("\n");

        $printer -> setEmphasis(true);
        //$printer -> graphics($bar3_big);
        $printer -> bitImage($bar3_big);
        $printer->text("\n");

        $lastElement = end($bet_slips_info);

        foreach ($bet_slips_info as $key => $value)
        {   
            $printer -> setEmphasis(false);
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $competition = $value->country.','.$value->competition;
            $printer->text($competition);
            $printer->text("\n");

            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $match = $value->game_id.' '.$value->name;
            $printer->text($match);
            $printer->text("\n");

            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $start_time = $value->date; 
            $start_time = date_create($start_time);
            date_timezone_set($start_time, timezone_open($this->configs->getTimezone()));
            $start_time = $start_time->format('Y-m-d H:i:s');
            $printer->text($start_time);
            $printer->text("\n");

            // calculate padding
            $market_length = strlen($value->market);
            $pick_length = strlen($value->pick);

            $total_length = ($market_length + $pick_length + 3);
            $padding = (40 - $total_length);

            $printer -> setEmphasis(true);
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $pick = $value->market.' : '.$value->pick." ".str_pad(" ", $padding).$value->odd_value;
            $printer->text($pick);
            $printer->text("\n");
            //$printer->text($key+1);

            if(!($value == $lastElement)) { 
                $br = "_______________________________________________";
                $br = str_pad($br, 45);
                $printer->text($br);
                $printer->text("\n");
            }


        }

        $printer->text("\n");
        $printer -> setEmphasis(true);
        //$printer -> graphics($bar3_big);
        $printer -> bitImage($bar3_big);
        $printer->text("\n");

        $total_odd_length = strlen($total_odd);
        $padding = (20 - $total_odd_length);

        $total_odds_title = str_pad(" ", 10).$this->configs->getString('TOTAL_ODDS_LABEL');
        $total_odd_value = str_pad(" ", $padding).$total_odd;
        $total_odds_value = $total_odds_title.$total_odd_value;
        $printer->text($total_odds_value);
        $printer->text("\n");


        $language = $this->configs->getLanguage();
        if($language == 'fr'):
            $total_odds_padding_length = 16;
        else:
            $total_odds_padding_length = 15;
        endif;
        $bet_amount_length = strlen($bet_amount);
        $padding = ($total_odds_padding_length - $bet_amount_length);

        $total_stake = str_pad(" ", 9).$this->configs->getString('AMOUNT_TITLE');
        $total_stake_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bet_amount;
        $total_stake_value_text = $total_stake.$total_stake_value;
        $printer->text($total_stake_value_text);
        $printer->text("\n");

        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(false);
        $printer->text("\n");

        $possible_win_length = strlen($possible_win);
        $padding = (10 - $possible_win_length);
        $printer -> setEmphasis(true);

        // show tax info
        if($this->configs->getTaxEnabled() == "true"):

            $taxRate        = $this->configs->getTaxRate();
            $stake          = round(($bet_amount / $taxRate),2);
            $tax            = ($bet_amount - $stake);
            //$possible_win   = round(($stake * $total_odd),2);
            $possible_win   = round(($bet_amount * $total_odd),2);

            $totalWinnings  = round(($possible_win / $taxRate),2);
            $withholdingTax = ($possible_win - $totalWinnings); 
            //$bonus          = round(($tax * $total_odd), 2);
            //$bonus          = round(($bet_amount - ($bet_amount / $taxRate)),2);
            $bonus          = 0;


            // $withholdingTax = round(($taxRate * $possible_win), 2);
            // $totalWinnings = $possible_win;
            // $possible_win = ($possible_win - ($taxRate * $possible_win));

            // new tax calculation for winnings above 100,000 kwacha
            // 5% tax on winnings above 100,000
            // withholding tax = (possible win - 100,000) * 0.05

            if($totalWinnings > $this->configs->getMaxTotalWinnings()):
                $totalWinnings = $this->configs->getMaxTotalWinnings();
            endif;
            
            $withholdingTax = round(($taxRate * ($possible_win - 100000)), 2);
            $totalWinnings = $possible_win;
            $possible_win = ($possible_win - ($taxRate *  ($possible_win - 100000)));

            if($possible_win > $this->configs->getMaxTotalWinnings()):
                $possible_win = $this->configs->getMaxPayout();
            endif;



            //$totalWinnings  = round(($totalWinnings + $bonus),2);

            // check if possible win is below 100000 kwacha remove tax
            if($possible_win < 100000):
                $stake          = round(($bet_amount / $taxRate),2);
                $tax            = ($bet_amount - $stake);
                //$possible_win   = round(($stake * $total_odd),2);
                $possible_win   = round(($bet_amount * $total_odd),2);
                $withholdingTax = 0;
                $bonus          = 0;
                $totalWinnings  = $possible_win;
                $possible_win  = $possible_win;
            endif;   

            if($this->configs->getVatEnabled() == "true"):
                $tax_text = str_pad(" ", 12).$this->configs->getString('VAT_LABEL');
                $tax_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$tax;
                $tax_value_text = $tax_text.$tax_value;
                $printer->text($tax_value_text);
                $printer->text("\n");
            endif;

            $stake_text = str_pad(" ", 11).$this->configs->getString('BET_AMOUNT_LABEL');
            $stake_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bet_amount;
            $stake_value_text = $stake_text.$stake_value;
            $printer->text($stake_value_text);
            $printer->text("\n");

            // check if possible payout
            if($possible_win > $this->configs->getMaxPayout()):
                $possible_win = $this->configs->getMaxTotalWinnings();
            endif;
            
            $total_winnings_text = str_pad(" ", 7).$this->configs->getString('TOTAL_WINNINGS_LABEL');
            $total_winnings_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$totalWinnings;
            $total_winnings_value_text = $total_winnings_text.$total_winnings_value;
            $printer->text($total_winnings_value_text);
            $printer->text("\n");

            // check if possible payout
            if($withholdingTax > $this->configs->getMaxWinningsTax()):
                $withholdingTax = $this->configs->getMaxWinningsTax();
            endif;

            // check if possible payout is greater than 5,000,000
            if($totalWinnings > $this->configs->getMaxTotalWinnings()):
                $totalWinnings = $this->configs->getMaxTotalWinnings();

                // show maximum payout amount on receipt
                $maximum_payout_text = str_pad(" ", 7).$this->configs->getString('MAXIMUM_PAYOUT_LABEL');
                $maximum_payout_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$totalWinnings;
                $maximum_payout_value_text = $maximum_payout_text.$maximum_payout_value;
                $printer->text($maximum_payout_value_text);
                $printer->text("\n");

            endif;
            
            $tax_text = str_pad(" ", 6).$this->configs->getString('WITHHOLDING_TAX');
            $tax_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$withholdingTax;
            $tax_value_text = $tax_text.$tax_value;
            $printer->text($tax_value_text);
            $printer->text("\n");

            if($this->configs->getAccaBonusEnabled() == "true"):
                $bonus_text = str_pad(" ", 8).$this->configs->getString('BONUS_LABEL');
                $bonus_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bonus;
                $bonus_value_text = $bonus_text.$bonus_value;
                $printer->text($bonus_value_text);
                $printer->text("\n");

                $acca_bonus_text = str_pad(" ", 8).$this->configs->getString('ACCA_BONUS_LABEL').'('.$current_acca_bonus_percentage.'%)';
                $acca_bonus_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$acca_bonus;
                $acca_bonus_value_text = $acca_bonus_text.$acca_bonus_value;
                $printer->text($acca_bonus_value_text);
                $printer->text("\n");
            endif;
        endif;

        // // check if possible payout is greater than 5,000,000
        // if($totalWinnings > $this->configs->getMaxTotalWinnings()):
        //     $totalWinnings = $this->configs->getMaxTotalWinnings();

        //     // show maximum payout amount on receipt
        //     $maximum_payout_text = str_pad(" ", 7).$this->configs->getString('MAXIMUM_PAYOUT_LABEL');
        //     $maximum_payout_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$totalWinnings;
        //     $maximum_payout_value_text = $maximum_payout_text.$maximum_payout_value;
        //     $printer->text($maximum_payout_value_text);
        //     $printer->text("\n");

        // endif;

        if($this->configs->getAccaBonusEnabled() == "true"):
            // add acca bonus to final possible payout
            $possible_win_text = str_pad(" ", 6).$this->configs->getString('POSSIBLE_PAYOUT_LABEL');
            $possible_win_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".round(($possible_win + $acca_bonus) ,2);
            $possible_win_value_text = $possible_win_text.$possible_win_value;
            $printer->text($possible_win_value_text);
            $printer->text("\n");
        else:
            $possible_win_text = str_pad(" ", 6).$this->configs->getString('POSSIBLE_PAYOUT_LABEL');
            $possible_win_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".round(($possible_win) ,2);
            $possible_win_value_text = $possible_win_text.$possible_win_value;
            $printer->text($possible_win_value_text);
            $printer->text("\n");
        endif;

        // $possible_win_text = str_pad(" ", 6).$this->configs->getString('POSSIBLE_PAYOUT_LABEL');
        // $possible_win_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$totalWinnings;
        // $possible_win_value_text = $possible_win_text.$possible_win_value;
        // $printer->text($possible_win_value_text);
        // $printer->text("\n");



        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('GOOD_LUCK_TEXT'));
        $printer->text("\n");
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(false);


        $printer -> setEmphasis(true);
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text("\n\n");
        $printer -> text($this->configs->getString('NB_TEXT')."\n");
        $printer -> text($this->configs->getString('VOID_BETS_MESSAGE')."\n");
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);
        $printer -> setEmphasis(false);

        /* Cut the receipt and open the cash drawer */
        $printer -> cut();
        $printer -> pulse();
        $printer -> close();
    }

    public function print_jackpot_betslip($bet_slips_info, $bet) {

        $shop_name     = $bet->shop_name;
        $bet_id        = $bet->bet_id;
        $bet_amount    = $bet->bet_amount;
        $jackpot_prize = $bet->jackpot_prize;
        $date_created  = $bet->date_created;
        $date          = $bet->date;


        //$date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        $logo       = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $bar_big    = EscposImage::load("/var/www/html/printer_api/bar-big.png", false);
        $bar2_big   = EscposImage::load("/var/www/html/printer_api/bar2-big.png", false);
        $bar3_big   = EscposImage::load("/var/www/html/printer_api/bar3-big.png", false);
        $printer    = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);


        $printer -> setTextSize(1, 1);
        $printer->text("\n\n");
        /*$printer -> setEmphasis(true);
        $printer->text($this->configs->getString('JUKWAA_LA_MABINGWA'));
        $printer->text("\n\n");
        $printer -> setEmphasis(false);*/

        /*$printer -> setEmphasis(true);
        $printer->text($this->configs->getString('JUKWAA_LA_MABINGWA'));
        $printer->text("\n\n");
        $printer -> setEmphasis(false);*/


        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $shop_name = $this->configs->getString('SHOP_NAME_LABEL').$shop_name;
        $printer->text($shop_name);
        $printer->text("\n");

        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $bet_id = $this->configs->getString('WEEKLY_JACKPOT_BET_ID_LABEL').$bet_id;
        $printer->text($bet_id);
        $printer->text("\n");

        $date_created = date_create($date_created);
        date_timezone_set($date_created, timezone_open($this->configs->getTimezone()));
        $date_created = $date_created->format('Y-m-d H:i:s');
        
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $date_created = $this->configs->getString('DATE_LABEL').$date_created;
        $printer->text($date_created);
        $printer->text("\n");

        $printer -> setEmphasis(true);
        //$printer -> graphics($bar3_big);
        $printer -> bitImage($bar3_big);
        $printer->text("\n");

        $lastElement = end($bet_slips_info);

        foreach ($bet_slips_info as $key => $value)
        {   
            $printer -> setEmphasis(false);
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $competition = $value->country.','.$value->competition;
            $printer->text($competition);
            $printer->text("\n");

            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $match = $value->game_id.' '.$value->name;
            $printer->text($match);
            $printer->text("\n");

            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $start_time = $value->date; 
            error_log("betslip start time :: ".$start_time);
            $start_time = date_create($start_time);
            date_timezone_set($start_time, timezone_open($this->configs->getTimezone()));
            $start_time = $start_time->format('Y-m-d H:i:s');
            error_log("kinshasa betslip start time :: ".$start_time);
            $printer->text($start_time);
            $printer->text("\n");

            // calculate padding
            $market_length = strlen($value->market);
            $pick_length = strlen($value->pick);

            $total_length = ($market_length + $pick_length + 3);
            $padding = (40 - $total_length);

            $printer -> setEmphasis(true);
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            //$pick = $value->market.' : '.$value->pick." ".str_pad(" ", $padding).$value->odd_value;
            $pick = $value->market.' : '.$value->pick." ".str_pad(" ", $padding);
            $printer->text($pick);
            $printer->text("\n");
            //$printer->text($key+1);

            if(!($value == $lastElement)) { 
                $br = "_______________________________________________";
                $br = str_pad($br, 45);
                $printer->text($br);
                $printer->text("\n");
            }

        }

        $printer->text("\n");
        $printer -> setEmphasis(true);
        //$printer -> graphics($bar3_big);
        $printer -> bitImage($bar3_big);
        $printer->text("\n");

        $total_odds_padding_length = 12;
        $bet_amount_length = strlen($bet_amount);
        $padding = ($total_odds_padding_length - $bet_amount_length);

        $total_stake = str_pad(" ", 9).$this->configs->getString('AMOUNT_TITLE');
        $total_stake_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bet_amount;
        $total_stake_value_text = $total_stake.$total_stake_value;
        $printer->text($total_stake_value_text);
        $printer->text("\n");

        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(false);
        $printer->text("\n");

        $possible_win_length = strlen($possible_win);
        $padding = (10 - $possible_win_length);
        $printer -> setEmphasis(true);


        // show tax info
        if($this->configs->getTaxEnabled() == "true"):

            $taxRate        = $this->configs->getTaxRate();
            $stake          = round(($bet_amount / $taxRate),2);
            $tax            = ($bet_amount - $stake);
            //$possible_win   = round(($stake * $total_odd),2);

            $totalWinnings  = round(($jackpot_prize / $taxRate),2);
            $withholdingTax = ($jackpot_prize - $totalWinnings); 
            //$bonus          = round(($tax * $total_odd), 2);
            //$bonus          = round(($bet_amount - ($bet_amount / $taxRate)),2);
            $bonus          = 0;
            $totalWinnings  = round(($totalWinnings + $bonus),2);

            if($this->configs->getVatEnabled() == "true"):
                $tax_text = str_pad(" ", 5).$this->configs->getString('VAT_LABEL');
                $tax_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$tax;
                $tax_value_text = $tax_text.$tax_value;
                $printer->text($tax_value_text);
                $printer->text("\n");
            endif;

            $stake_text = str_pad(" ", 4).$this->configs->getString('BET_AMOUNT_LABEL');
            $stake_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$stake;
            $stake_value_text = $stake_text.$stake_value;
            $printer->text($stake_value_text);
            $printer->text("\n");

            
            $total_winnings_text = str_pad(" ", 4).$this->configs->getString('TOTAL_WINNINGS_LABEL');
            $total_winnings_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$jackpot_prize;
            $total_winnings_value_text = $total_winnings_text.$total_winnings_value;
            $printer->text($total_winnings_value_text);
            $printer->text("\n");

            $tax_text = str_pad(" ", 3).$this->configs->getString('WITHHOLDING_TAX');
            $tax_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$withholdingTax;
            $tax_value_text = $tax_text.$tax_value;
            $printer->text($tax_value_text);
            $printer->text("\n");

            /*$bonus_text = str_pad(" ", 8).$this->configs->getString('BONUS_LABEL');
            $bonus_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bonus;
            $bonus_value_text = $bonus_text.$bonus_value;
            $printer->text($bonus_value_text);
            $printer->text("\n");*/
        endif;

        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(true);
        $printer->text("\n");


        $possible_win_text = str_pad(" ", 3).$this->configs->getString('POSSIBLE_PAYOUT_LABEL');
        $possible_win_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$totalWinnings;
        $possible_win_value_text = $possible_win_text.$possible_win_value;
        $printer->text($possible_win_value_text);
        $printer->text("\n");

        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('GOOD_LUCK_TEXT'));
        $printer->text("\n");
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(false);


        $printer -> setEmphasis(true);
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text("\n\n");
        $printer -> text($this->configs->getString('WEEKLY_JACKPOT_NB_LABEL')."\n\n");
        $printer -> text($this->configs->getString('NB_TEXT')."\n");
        $printer -> text($this->configs->getString('VOID_BETS_MESSAGE')."\n");
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);
        $printer -> setEmphasis(false);

        /* Cut the receipt and open the cash drawer */
        $printer -> cut();
        $printer -> pulse();
        $printer -> close();
    }

    public function print_sababisha_betslip($bet_slips_info, $bet) {

        $shop_name     = $bet->shop_name;
        $bet_id        = $bet->bet_id;
        $bet_amount    = $bet->bet_amount;
        $jackpot_prize = $bet->jackpot_prize;
        $date_created  = $bet->date_created;
        $date          = $bet->date;


        //$date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        $logo       = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $bar_big    = EscposImage::load("/var/www/html/printer_api/bar-big.png", false);
        $bar2_big   = EscposImage::load("/var/www/html/printer_api/bar2-big.png", false);
        $bar3_big   = EscposImage::load("/var/www/html/printer_api/bar3-big.png", false);
        $printer    = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);


        $printer -> setTextSize(1, 1);
        $printer->text("\n\n");
        /*$printer -> setEmphasis(true);
        $printer->text($this->configs->getString('JUKWAA_LA_MABINGWA'));
        $printer->text("\n\n");
        $printer -> setEmphasis(false);*/

        /*$printer -> setEmphasis(true);
        $printer->text($this->configs->getString('JUKWAA_LA_MABINGWA'));
        $printer->text("\n\n");
        $printer -> setEmphasis(false);*/

        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $shop_name = $this->configs->getString('SHOP_NAME_LABEL').$shop_name;
        $printer->text($shop_name);
        $printer->text("\n");

        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $bet_id = $this->configs->getString('DAILY_JACKPOT_BET_ID_LABEL').$bet_id;
        $printer->text($bet_id);
        $printer->text("\n");

        $date_created = date_create($date_created);
        date_timezone_set($date_created, timezone_open($this->configs->getTimezone()));
        $date_created = $date_created->format('Y-m-d H:i:s');
        
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $date_created = $this->configs->getString('DATE_LABEL').$date_created;
        $printer->text($date_created);
        $printer->text("\n");

        $printer -> setEmphasis(true);
        //$printer -> graphics($bar3_big);
        $printer -> bitImage($bar3_big);
        $printer->text("\n");

        $lastElement = end($bet_slips_info);

        foreach ($bet_slips_info as $key => $value)
        {   
            $printer -> setEmphasis(false);
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $competition = $value->country.','.$value->competition;
            $printer->text($competition);
            $printer->text("\n");

            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $match = $value->game_id.' '.$value->name;
            $printer->text($match);
            $printer->text("\n");

            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $start_time = $value->date; 
            error_log("betslip start time :: ".$start_time);
            $start_time = date_create($start_time);
            date_timezone_set($start_time, timezone_open($this->configs->getTimezone()));
            $start_time = $start_time->format('Y-m-d H:i:s');
            error_log("kinshasa betslip start time :: ".$start_time);
            $printer->text($start_time);
            $printer->text("\n");

            // calculate padding
            $market_length = strlen($value->market);
            $pick_length = strlen($value->pick);

            $total_length = ($market_length + $pick_length + 3);
            $padding = (40 - $total_length);

            $printer -> setEmphasis(true);
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            //$pick = $value->market.' : '.$value->pick." ".str_pad(" ", $padding).$value->odd_value;
            $pick = $value->market.' : '.$value->pick." ".str_pad(" ", $padding);
            $printer->text($pick);
            $printer->text("\n");
            //$printer->text($key+1);

            if(!($value == $lastElement)) { 
                $br = "_______________________________________________";
                $br = str_pad($br, 45);
                $printer->text($br);
                $printer->text("\n");
            }

        }

        $printer->text("\n");
        $printer -> setEmphasis(true);
        //$printer -> graphics($bar3_big);
        $printer -> bitImage($bar3_big);
        $printer->text("\n");

        $total_odds_padding_length = 12;
        $bet_amount_length = strlen($bet_amount);
        $padding = ($total_odds_padding_length - $bet_amount_length);

        $total_stake = str_pad(" ", 9).$this->configs->getString('AMOUNT_TITLE');
        $total_stake_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bet_amount;
        $total_stake_value_text = $total_stake.$total_stake_value;
        $printer->text($total_stake_value_text);
        $printer->text("\n");

        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(false);
        $printer->text("\n");

        $possible_win_length = strlen($possible_win);
        $padding = (10 - $possible_win_length);
        $printer -> setEmphasis(true);


        // show tax info
        if($this->configs->getTaxEnabled() == "true"):

            $taxRate        = $this->configs->getTaxRate();
            $stake          = round(($bet_amount / $taxRate),2);
            $tax            = ($bet_amount - $stake);
            //$possible_win   = round(($stake * $total_odd),2);

            $totalWinnings  = round(($jackpot_prize / $taxRate),2);
            $withholdingTax = ($jackpot_prize - $totalWinnings); 
            //$bonus          = round(($tax * $total_odd), 2);
            //$bonus          = round(($bet_amount - ($bet_amount / $taxRate)),2);
            $bonus          = 0;
            $totalWinnings  = round(($totalWinnings + $bonus),2);

            if($this->configs->getVatEnabled() == "true"):
                $tax_text = str_pad(" ", 8).$this->configs->getString('VAT_LABEL');
                $tax_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$tax;
                $tax_value_text = $tax_text.$tax_value;
                $printer->text($tax_value_text);
                $printer->text("\n");
            endif;

            $stake_text = str_pad(" ", 7).$this->configs->getString('BET_AMOUNT_LABEL');
            $stake_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$stake;
            $stake_value_text = $stake_text.$stake_value;
            $printer->text($stake_value_text);
            $printer->text("\n");

            
            $total_winnings_text = str_pad(" ", 4).$this->configs->getString('TOTAL_WINNINGS_LABEL');
            $total_winnings_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$jackpot_prize;
            $total_winnings_value_text = $total_winnings_text.$total_winnings_value;
            $printer->text($total_winnings_value_text);
            $printer->text("\n");

            $tax_text = str_pad(" ", 3).$this->configs->getString('WITHHOLDING_TAX');
            $tax_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$withholdingTax;
            $tax_value_text = $tax_text.$tax_value;
            $printer->text($tax_value_text);
            $printer->text("\n");

            /*$bonus_text = str_pad(" ", 8).$this->configs->getString('BONUS_LABEL');
            $bonus_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$bonus;
            $bonus_value_text = $bonus_text.$bonus_value;
            $printer->text($bonus_value_text);
            $printer->text("\n");*/
        endif;

        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(true);
        $printer->text("\n");


        $possible_win_text = str_pad(" ", 3).$this->configs->getString('POSSIBLE_PAYOUT_LABEL');
        $possible_win_value = str_pad(" ", $padding).$this->configs->getCurrency()." ".$totalWinnings;
        $possible_win_value_text = $possible_win_text.$possible_win_value;
        $printer->text($possible_win_value_text);
        $printer->text("\n");

        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('GOOD_LUCK_TEXT'));
        $printer->text("\n");
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> setEmphasis(true);
        $br = "_______________________________________________";
        $br = str_pad($br, 45);
        $printer->text($br);
        $printer -> setEmphasis(false);


        $printer -> setEmphasis(true);
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text("\n\n");
        $printer -> text($this->configs->getString('DAILY_JACKPOT_NB_LABEL')."\n\n");
        $printer -> text($this->configs->getString('NB_TEXT')."\n");
        $printer -> text($this->configs->getString('VOID_BETS_MESSAGE')."\n");
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);
        $printer -> setEmphasis(false);

        /* Cut the receipt and open the cash drawer */
        $printer -> cut();
        $printer -> pulse();
        $printer -> close();
    }


    public function print_cancelled_bet_receipt($bet_id, $bet_amount) {

        $items = array(
            new CancelBetReceiptStyle($bet_amount, $this->configs->getString('SIGNATURE_LABEL'), "----------------------------"),
        );

        $date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        /* Start the printer */
        $logo = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $printer = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);
        $printer -> text("\n");

        /* Header */
        $printer -> text($date."\n");
        $printer -> selectPrintMode();
        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('CANCELLED_BET_RECEIPT_TITLE')."\n\n");
        $printer -> text($this->configs->getString('BET_ID_LABEL').$bet_id.".\n\n");
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $bet_amount = $this->configs->getString('AMOUNT_LABEL');
        $one = str_pad($bet_amount, 24);
        $printer->setEmphasis(true);
        $printer->text($one."\n\n");

        /* Print Items */
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $printer -> setEmphasis(true);
        foreach ($items as $item) {
           $printer -> text($item);
        }

        /* Footer */
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);

        $printer -> cut();
        $printer -> pulse();
        $printer -> close();
    }

    public function print_payment_receipt($bet_id, $bet_amount, $acca_bonus, $current_acca_bonus_percentage) {

        $items = array(
            new PaymentReceiptStyle($bet_id, $bet_amount, $acca_bonus, $current_acca_bonus_percentage, $this->configs->getString('SIGNATURE_LABEL'), "----------------------------"),
        );

        $date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        /* Start the printer */
        $logo = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $printer = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);
        $printer -> text("\n");

        /* Header */
        $printer -> text($date."\n");
        $printer -> selectPrintMode();
        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('PAYMENT_RECEIPT_TITLE')."\n\n");
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        // $bet_id_title       = $this->configs->getString('BET_ID_TITLE');
        // $amount_title       = $this->configs->getString('AMOUNT_TITLE');
        // $acca_bonus_title   = $this->configs->getString('ACCA_BONUS_LABEL');
        // $one                = str_pad($bet_id_title, 24);
        // $two                = str_pad($amount_title, 24);
        // $three              = str_pad($acca_bonus_title, 24);
        // $printer->setEmphasis(true);
        // $printer->text($one.$two.$three . "\n");

        /* Print Items */
        // $printer -> setJustification(Printer::JUSTIFY_LEFT);
        // $printer -> setEmphasis(true);
        // foreach ($items as $item) {
        //    $printer -> text($item);
        // }


        $bet_id_text = str_pad(" ", 1).$this->configs->getString('BET_ID_TITLE')." :: ";
        $bet_id_value = str_pad(" ", $padding).$bet_id;
        $bet_id_value_text = $bet_id_text.$bet_id_value;
        $printer->text($bet_id_value_text);
        $printer->text("\n");


        $paid_amount_text = str_pad(" ", 2).$this->configs->getString('AMOUNT_TITLE');
        $paid_amount_value = str_pad(" ", $padding).$bet_amount;
        $paid_amount_value_text = $paid_amount_text.$paid_amount_value;
        $printer->text($paid_amount_value_text);
        $printer->text("\n");

        if($this->configs->getAccaBonusEnabled() == "true"):

            $acca_bonus_text = str_pad(" ", 2).$this->configs->getString('ACCA_BONUS_LABEL')." :: ";
            $acca_bonus_value = str_pad(" ", $padding).$acca_bonus;
            $acca_bonus_value_text = $acca_bonus_text.$acca_bonus_value;
            $printer->text($acca_bonus_value_text);
            $printer->text("\n");


            $final_payout_text = str_pad(" ", 2).$this->configs->getString('FINAL_PAYOUT_LABEL')." :: ";
            $final_payout_value = str_pad(" ", $padding).round(($acca_bonus + $bet_amount), 2);
            $final_payout_value_text = $final_payout_text.$final_payout_value;
            $printer->text($final_payout_value_text);
            $printer->text("\n");

        endif;

        $final_payout_text = str_pad(" ", 2).$this->configs->getString('FINAL_PAYOUT_LABEL')." :: ";
        $final_payout_value = str_pad(" ", $padding).round(($bet_amount), 2);
        $final_payout_value_text = $final_payout_text.$final_payout_value;
        $printer->text($final_payout_value_text);
        $printer->text("\n");

        $signature_text = str_pad(" ", 3).$this->configs->getString('SIGNATURE_LABEL');
        $signature_value = str_pad(" ", $padding)."----------------------------";
        $signature_value_text = $signature_text.$signature_value;
        $printer->text($signature_value_text);
        $printer->text("\n");


        /* Footer */
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);

        $printer -> cut();
        $printer -> pulse();
        //$printer -> close();
        
    }

    public function print_withdrawal_receipt($msisdn, $amount, $reference_id) {

        $items = array(
            new WithdrawalReceiptStyle($msisdn, $amount, $this->configs->getString('SIGNATURE_LABEL'), "----------------------------"),
        );

        $date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        /* Start the printer */
        $logo = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $printer = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);
        $printer -> text("\n");

        /* Header */
        $printer -> text($date."\n");
        $printer -> selectPrintMode();
        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('WITHDRAWAL_RECEIPT_TITLE')."\n");
        $printer -> text($this->configs->getString('WITHDRAWAL_CODE_LABEL').$reference_id."\n");
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $phone_number_title = $this->configs->getString('PHONE_NUMBER_LABEL');
        $amount_title = $this->configs->getString('AMOUNT_LABEL');
        $one = str_pad($phone_number_title, 24);
        $two = str_pad($amount_title, 24);
        $printer->setEmphasis(true);
        $printer->text($one.$two . "\n");

        /* Print Items */
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $printer -> setEmphasis(true);
        foreach ($items as $item) {
           $printer -> text($item);
        }

        /* Footer */
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);

        $printer -> cut();
        $printer -> pulse();
        $printer -> close();
    }

    public function print_deposit_receipt($msisdn, $amount) {

        $items = array(
            new DepositReceiptStyle($msisdn, $amount, $this->configs->getString('SIGNATURE_LABEL'), "----------------------------"),
        );

        $date = date('Y-m-d H:i:s');
        $date = date_create($date);
        date_timezone_set($date, timezone_open($this->configs->getTimezone()));
        $date = $date->format('Y-m-d H:i:s');

        /* Start the printer */
        $logo = EscposImage::load("/var/www/html/printer_api/".$this->configs->getLogo(), false);
        $printer = new Printer($this->connector);

        /* Print Betika Logo */
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        //$printer -> graphics($logo);
        //$logo = EscposImage::load(dirname(FILE) . "/logo.png", false);
        $printer -> bitImage($logo);
        $printer -> text("\n");

        /* Header */
        $printer -> text($date."\n");
        $printer -> selectPrintMode();
        $printer -> text($this->configs->getString('ACCENTED_WORDS_PRINTING')."\n");
        $printer -> text($this->configs->getString('DEPOSIT_RECEIPT_TITLE')."\n");
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $phone_number_title = $this->configs->getString('PHONE_NUMBER_LABEL');
        $amount_title = $this->configs->getString('AMOUNT_LABEL');
        $one = str_pad($phone_number_title, 24);
        $two = str_pad($amount_title, 24);
        $printer->setEmphasis(true);
        $printer->text($one.$two . "\n");

        /* Print Items */
        $printer -> setJustification(Printer::JUSTIFY_LEFT);
        $printer -> setEmphasis(true);
        foreach ($items as $item) {
           $printer -> text($item);
        }

        /* Footer */
        $printer -> feed();
        $printer -> setJustification(Printer::JUSTIFY_CENTER);
        $printer -> text($this->configs->getString('THANK_YOU_TEXT')."\n");
        $printer -> text($this->configs->getString('TERMS_AND_CONDITIONS_TEXT')."\n");
        $printer -> feed(2);

        $printer -> cut();
        $printer -> pulse();
        $printer -> close();

    }
}
